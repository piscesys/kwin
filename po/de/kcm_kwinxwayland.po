# German translations for kwin package.
# Copyright (C) 2022 This file is copyright:
# This file is distributed under the same license as the kwin package.
# Jannick Kuhr <opensource@kuhr.org>, 2023.
# Frederik Schwarzer <schwarzer@kde.org>, 2023.
#
# Automatically generated, 2022.
msgid ""
msgstr ""
"Project-Id-Version: kwin\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-06-07 02:19+0000\n"
"PO-Revision-Date: 2023-06-25 17:14+0200\n"
"Last-Translator: Frederik Schwarzer <schwarzer@kde.org>\n"
"Language-Team: German <kde-i18n-de@kde.org>\n"
"Language: de\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 22.12.3\n"

#: ui/main.qml:47
#, kde-format
msgid ""
"Legacy X11 apps require the ability to read keystrokes typed in other apps "
"for features that are activated using global keyboard shortcuts. This is "
"disabled by default for security reasons. If you need to use such apps, you "
"can choose your preferred balance of security and functionality here."
msgstr ""
"Klassische X11-Anwendungen benötigen Zugriff auf Tastendrücke, die in "
"anderen Anwendungen getippt werden. um Funktionen zu aktivieren, die über "
"globale Kurzbefehle ausgelöst werden. Daher ist es standardmäßig aus "
"Sicherheitsgründen ausgeschaltet. Wenn Sie solche Anwendungen verwenden, "
"können Sie hier Ihren eigenen Kompromiss aus Sicherheit und Funktionalität "
"wählen."

#: ui/main.qml:64
#, kde-format
msgid "Allow legacy X11 apps to read keystrokes typed in all apps:"
msgstr ""
"Klassischen X11-Anwendungen Zugriff auf Tastendrücke in allen Anwendungen "
"gewähren:"

#: ui/main.qml:65
#, kde-format
msgid "Never"
msgstr "Niemals"

#: ui/main.qml:68
#, kde-format
msgid "Only Meta, Control, Alt, and Shift keys"
msgstr "Nur die Tasten Meta, Strg, Alt und Umschalt"

#: ui/main.qml:71
#, kde-format
msgid "All keys, but only while Meta, Ctrl, Alt, or Shift keys are pressed"
msgstr ""
"Alle Tasten, aber nur wenn eine der Tasten Meta, Strg, Alt oder Umschalt "
"gedrückt ist"

#: ui/main.qml:75
#, kde-format
msgid "Always"
msgstr "Immer"

#: ui/main.qml:82
#, kde-format
msgid ""
"Note that using this setting will reduce system security to that of the X11 "
"session by permitting malicious software to steal passwords and spy on the "
"text that you type. Make sure you understand and accept this risk."
msgstr ""
"Bitte beachten Sie, dass diese Einstellung die Systemsicherheit auf die "
"Stufe von einer X11-Sitzung schwächt und es bösartiger Software erlaubt, "
"Passwörter zu stehlen und Text den Sie tippen abzuhören. Seien Sie sich der "
"Risiken bewusst."
